Game = Object:extend()

-- Definindo força da gravidade
local GRAVITY = 14.8

---
local texto_debug = ""

function Game:new()
    self.fase = "maps/test_map.lua"
    self.entidades = {}
    self.debug = {}
    self.player = {}
    self.inimigo1 = {}
    self.inimigo2 = {}
    self.inimigo3 = {}
    --
    self:carregaArquivos()
    self:carregaFase()

end

function Game:carregaArquivos()
    self.barack = love.graphics.newImage("sprites/barack_andando.png")
    self.crate = love.graphics.newImage("sprites/caixa.png")
    self.inimigo = love.graphics.newImage("sprites/inimigo.png")
end

-- Função que carrega a fase inicial do jogo
-- Configura a Física 
-- Organiza as entidades (personagem principal e de mais itens) no world (cenário)
function Game:carregaFase()
    self.entidades = {}
    self:configuraFisica()
    self.map = sti(self.fase, {"bump"})
    self.map:bump_init(self.world)

    for k, object in pairs(self.map.objects) do
        -- carregando o personagem Barack
        if object.name == "player_spawn" then
            self.player = Barack(object.x, object.y, 32, 32, self.barack,
                                 self.world, 200, 64, 100)
            table.insert(self.entidades, self.player)
            -- carregando o objeto caixa
        elseif object.name == "box" then
            local caixa = EntidadeDinamica(object.x, object.y, 32, 32,
                                           self.crate, self.world, 200, 64,
                                           "ent_crate", 10)
            table.insert(self.entidades, caixa)
            -- carregando o objeto prego
        elseif object.name == "spike" then
            local pregos = Entidade(object.x, object.y, object.width,
                                    object.height, nil, self.world, "ent_spike")
            table.insert(self.entidades, pregos)
        elseif object.name == "inimigo1" then
            self.inimigo1 = EntidadeDinamica(object.x, object.y, 32, 32,
                                              self.inimigo, self.world, 200, 64,
                                              "ent_inimigo1", 100)
            table.insert(self.entidades, self.inimigo1)
       elseif object.name == "inimigo2" then
            self.inimigo2 = EntidadeDinamica(object.x, object.y, 32, 32,
                                              self.inimigo, self.world, 200, 64,
                                              "ent_inimigo2", 100)
            table.insert(self.entidades, self.inimigo2)
        elseif object.name == "inimigo3" then
            self.inimigo3 = EntidadeDinamica(object.x, object.y, 32, 32,
                                              self.inimigo, self.world, 200, 64,
                                              "ent_inimigo3", 100)
            table.insert(self.entidades, self.inimigo3)
            -- carregando portal, item da fase final
        elseif object.name == "level_end" then
            local fimFase = Entidade(object.x, object.y, 35, 50, nil,
                                     self.world, "ent_level_end")
            fimFase.nextMap = object.properties.next_map
            table.insert(self.entidades, fimFase)
        end
    end
    self.map:removeLayer("Objects")
end

function Game:configuraFisica()
    self.world = bump.newWorld()
    self.world.gravidade = GRAVITY
end

-- verificamos as colisões dos itens no cenário

function Game:checaColisoes(entidade, cols)
    local nome = entidade.nome
    entidade.retido = false

    for i, v in ipairs(cols) do
        local nomeAux = cols[i].other.nome
        texto_debug = #cols[i]

        -- se a colisão ocorrer entre a caixa e o protagonista            
        if (nome == "ent_player" or nome == "ent_crate") and nomeAux ==
            "ent_crate" and cols[i].normal.y ~= -1 then
            cols[i].other.direcao = entidade.direcao
            cols[i].other.xVel = (entidade.xVel)
            -- se não, se for entre o protagonista e o portal (que leva para outra fase)
            -- um novo mapa será carregado, ou seja, uma nova fase
        elseif nome == "ent_player" and nomeAux == "ent_level_end" then
            self.fase = cols[i].other.nextMap
            self:carregaFase()
            -- se for, entre o jogador e os pregos, a fase é reiniciada
        elseif nome == "ent_player" and nomeAux == "ent_spike" then
            self:carregaFase()
        elseif nome == "ent_player" and nomeAux == "ent_inimigo1" then
            self:carregaFase()
        elseif nome == "ent_player" and nomeAux == "ent_inimigo2" then
            self:carregaFase()
        elseif nome == "ent_player" and nomeAux == "ent_inimigo3" then
            self:carregaFase()
        end

        -- ajustamos os valores da velocidade horizontal e vertical
        if cols[i].normal.y == -1 then
            entidade.yVel = 0
            entidade.retido = true
        elseif cols[i].normal.y == 1 then
            entidade.yVel = -entidade.yVel / 4
        end

        if cols[i].normal.x ~= 0 and nomeAux == nil then
            entidade.xVel = 0
        end
    end
end
-- a animação do jogo será atualizada frequentemente com base no tempo dt
function Game:update(dt)
    self.map:update(dt)
    self:acoesTeclado(dt)

    if self.player.entAnimada ~= nil and self.player.animado then
        self.player.entAnimada.direcao = self.player.direcao
        self.player.entAnimada.maxVelX = self.player.maxVelX
        self.player.entAnimada.xVel = self.player.xVel
    end

    -- É necessário atualizar a fisica de todos os objetos/entidades do jogo
    for i = 1, #self.entidades do
        if self.entidades[i] ~= nil then
            if self.entidades[i]:is(EntidadeDinamica) then
                self.entidades[i]:atualizaFisicaEntidade(dt)
            end

            -- aqui "tentamos" mover os objetos
            -- nesse caso mover o personagem principal
            -- porém, se houver colisões a variavel 'cols' nos mostrará mais 
            -- informações a respeito
            self.entidades[i].x, self.entidades[i].y, cols =
                self.world:move(self.entidades[i], self.entidades[i].x,
                                self.entidades[i].y)

            -- para chechar as colisões
            -- enviamos 'cols' para o método chacarColisoes
            self:checaColisoes(self.entidades[i], cols)
        end
    end

    -- self.player:update(dt)

end

-- Essa função gerencia as ações do teclado
function Game:acoesTeclado(dt)
    if love.keyboard.isDown("d") then
        self.player:moverParaDireita(dt)
        self.inimigo1:saltar(300)
        self.inimigo2:saltar(400)
        self.inimigo3:saltar(100)
    elseif love.keyboard.isDown("a") then
        self.player:moverParaEsquerda(dt)
    end

    if love.keyboard.isDown("w") then self.player:pular(dt) end

    -- ação para segurar um objeto
    -- no exemplo, barack agarra uma caixa

    if love.keyboard.isDown("s") then
        if self.player.animado == false then
            for i = 1, #self.entidades do
                if self.player:segurarObjeto(self.entidades[i]) then
                    self.player.animado = true
                    self.player.entAnimada = self.entidades[i]
                    break
                end
            end
        end
    else
        self.player.animado = false
        self.player.entAnimada = nil
    end
end

-- Essa função é responsável por desenhar o jogo na tela
function Game:draw()
    self.map:draw()
    -- love.graphics.print("Vetor de Colisões: " .. #cols, 100, 12)
    -- love.graphics.print("Quantidade de Entidades no Jogo: " .. #self.entidades,
    --                   100, 24)
    -- love.graphics.print("Debug:" .. texto_debug, 100, 48)
    for i = 1, #self.entidades do self.entidades[i]:draw() end
end

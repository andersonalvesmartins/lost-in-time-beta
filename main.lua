-- Biblioteca para detectar colisões
bump = require "libs/bump"

-- Biblioteca que ajuda na renderização do mapa
sti = require "libs/sti"

-- Auxilia na implementação Orientada a Objetos
Object = require "libs/classic"

-- Biblioteca para auxiliar na criação das animações das entidades do jogo
anim8 = require 'libs/anim8'

-- local cols, GRAVITY, map, world
-- local entidades = {}
-- local game

-- arquivos do jogo
require "barack"
require "game"

background = ""

function love.load()
    love.window.setMode(1280, 690)    
    love.window.setTitle("Lost in Time")
    game = Game()
end

function love.update(dt) game:update(dt) end

function love.draw()   
    game:draw(dt)  
end

function love.mousepressed(x, y, button) game:carregaFase() end

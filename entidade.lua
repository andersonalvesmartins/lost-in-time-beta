-- @author - Joás Ramos
-- Entidade agora é uma classe
Entidade = Object:extend()

--construtor da classe Entidade
function Entidade:new(x, y, width, height, imagem, world, nome_entidade)
    self.x = x or 0
    self.y = y or 0
    self.w = width or 32
    self.h = height or 32
    self.imagem = imagem
    self.nome = nome_entidade or "entidade_base"

    if world ~= nil then world:add(self, self.x, self.y, self.w, self.h) end
end

-- método draw para auxiliar no desenho dos objetos do tipo Entidade
-- verifica se existe uma imagem associada ao objeto, caso não, o desenho não será realizado
function Entidade:draw()
    if self.imagem ~= nil then love.graphics.draw(self.imagem, self.x, self.y) end
end


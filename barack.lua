require "entidade_dinamica"

-- Barack é um dos personagens principais do jogo
-- Devido à suas ações ele é uma entidade dinamica
-- Sendo assim, seu comportamento de "EntidadeDinamica"
Barack = EntidadeDinamica:extend()

function Barack:new(x, y, width, height, imagem, world, maxVelX, maxVelY, speed)
    Barack.super.new(self, x, y, width, height, imagem, world, maxVelX, maxVelY,
                     "ent_player", 2)
    self.origX = x
    self.origY = 15
    self.speed = speed
    self.imagem = imagem
    self.animado = false 
    self.entAnimada = nil 
    self.origMaxVel = maxVelX

    local g = anim8.newGrid(32, 32, imagem:getWidth(), imagem:getHeight())

    self.animacaoNormal = anim8.newAnimation(g("1-8", 1), 0.1)
    self.animacaoInversa = anim8.newAnimation(g("1-8", 1), 0.1):flipH()

end

-- talvez não iremos utilizar
function Barack:update(dt)
    
    if self.xVel == 0 then
        self.animacaoNormal:gotoFrame(3)
        self.animacaoInversa:gotoFrame(3)
    end

    if self.retido == false then
        self.animado = false
        self.entAnimada = nil   
    end

    if self.animado then
        self.maxVelX = self.origMaxVel / 1.7
    else
        self.maxVelX = self.origMaxVel
    end 
end

function Barack:draw()
    if self.direcao == 1 then
        self.animacaoNormal:draw(self.imagem, self.x, self.y)
    elseif self.direcao == -1 then
        self.animacaoInversa:draw(self.imagem, self.x, self.y)
    end
end

function Barack:segurarObjeto(entidade)
    local yReq = math.abs(entidade.h - self.h) + 3
    local xReq = math.abs(entidade.w / 2 + self.w / 2) + 5
    local xDiff = entidade.x - self.x
    local yDiff = entidade.y - self.y

    if entidade.nome == "ent_crate" then
        if ((self.direcao > 0 and xDiff < 0) or
            (self.direcao < 0 and xDiff > 0)) then return false end
    end

    if entidade.nome == "ent_crate" and math.abs(yDiff) <= yReq and
        math.abs(xDiff) <= xReq and self.retido then return true end
end

function Barack:moverParaDireita(dt)
    self.xVel = self.xVel + self.speed
    self.animacaoInversa:update(dt)
    self.animacaoNormal:update(dt)
    self.direcao = 1
    if self.direcao == -1 then self.xVel = 0 end
end

function Barack:moverParaEsquerda(dt)
    self.xVel = self.xVel + self.speed
    self.direcao = -1
    self.animacaoInversa:update(dt)
    self.animacaoNormal:update(dt)

    if self.direcao == 1 then self.xVel = 0 end
end

function Barack:pular(dt)
    if self.retido == true then
        self.yVel = self.yVel - 300
        self.animado = false
        self.entAnimada = nil
    end
end

require "entidade"

-- @author - Joás Ramos
-- Essa classe representa (objetos) entidades dinâmicas
-- Por ser também uma entidade deve estender a classe entidade.lua
-- Possui dois construtores, pois em alguns casos poderá ser importante
-- informar a massa da entidade

EntidadeDinamica = Entidade:extend()

-- constante fisica usada para atualizar a velocidade dos objetos (entidades)
local constante_fisica = 65

-- construtor sem passar a massa do objeto
function EntidadeDinamica:new(x, y, width, height, image, world, maxVelX,
                              maxVelY)
    EntidadeDinamica.super.new(self, x, y, width, height, image, world,
                               "entidade_dinamica")
    self.maxVelX = maxVelX
    self.maxVelY = maxVelY
    self.xVel = 0
    self.yVel = 0
    self.direcao = 1
    self.retido = false

    GRAVITY = world.gravity or 9.8
end

-- construtor passando a massa e nome do objeto
function EntidadeDinamica:new(x, y, width, height, image, world, maxVelX,
                              maxVelY, nome_entidade, massa)
    EntidadeDinamica.super.new(self, x, y, width, height, image, world,
                               nome_entidade)
    self.maxVelX = maxVelX
    self.maxVelY = maxVelY
    self.xVel = 0
    self.yVel = 0
    self.direcao = 1
    self.retido = false
    self.massa = massa or 1

    GRAVITY = world.gravity or 9.8
end

-- essa função analisa colisões realizando controle
-- de velocidade no objeto
function EntidadeDinamica:checaColisoes(cols)
    self.retido = false
    for i, v in ipairs(cols) do
        if cols[i].normal.y == -1 then
            self.yVel = 0
            self.retido = true
        elseif cols[i].normal.y == 1 then
            self.yVel = -self.yVel / 4
        end
        if cols[i].normal.x ~= 0 then self.xVel = 0 end
    end
end

-- atualiza fisica do objeto com base no tempo (dt)
function EntidadeDinamica:atualizaFisicaEntidade(dt)
    if self.retido == true then
        self.xVel = self.xVel - 50 * dt * constante_fisica
    else
        self.xVel = self.xVel - (8 * self.massa) * dt * constante_fisica
    end

    self.yVel = self.yVel + (GRAVITY) * dt * constante_fisica

    if self.xVel > self.maxVelX then self.xVel = self.maxVelX end

    if self.xVel < 0 then self.xVel = 0 end

    self.x = self.x + self.direcao * self.xVel * dt
    self.y = self.y + self.yVel * dt

end

function EntidadeDinamica:saltar(vel)
    if self.retido == true then
        self.yVel = self.yVel - vel
        self.animado = false
        self.entAnimada = nil
    end
end
